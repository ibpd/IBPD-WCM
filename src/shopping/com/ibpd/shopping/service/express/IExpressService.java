package com.ibpd.shopping.service.express;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.shopping.entity.ExpressEntity;

public interface IExpressService extends IBaseService<ExpressEntity> {
	ExpressEntity getEntityByCode(String code);
}
