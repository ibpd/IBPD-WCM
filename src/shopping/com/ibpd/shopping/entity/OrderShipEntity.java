package com.ibpd.shopping.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import net.sf.json.JSONArray;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_ordership")
public class OrderShipEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_orderid",length=11,nullable=true)
	private Long orderid;
	@Column(name="f_shipname",length=45,nullable=true)
	private String shipname;
	@Column(name="f_shipaddress",length=200,nullable=true)
	private String shipaddress;
	@Column(name="f_provinceCode",length=45,nullable=true)
	private String provinceCode;
	@Column(name="f_province",length=45,nullable=true)
	private String province;
	@Column(name="f_cityCode",length=45,nullable=true)
	private String cityCode;
	@Column(name="f_city",length=45,nullable=true)
	private String city;
	@Column(name="f_areaCode",length=45,nullable=true)
	private String areaCode;
	@Column(name="f_area",length=45,nullable=true)
	private String area;
	@Column(name="f_phone",length=45,nullable=true)
	private String phone;
	@Column(name="f_tel",length=45,nullable=true)
	private String tel;
	@Column(name="f_zip",length=45,nullable=true)
	private String zip;
	@Column(name="f_sex",length=2,nullable=true)
	private String sex;
	@Column(name="f_remark",length=70,nullable=true)
	private String remark;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOrderid() {
		return orderid;
	}
	public void setOrderid(Long orderid) {
		this.orderid = orderid;
	}
	public String getShipname() {
		return shipname;
	}
	public void setShipname(String shipname) {
		this.shipname = shipname;
	}
	public String getShipaddress() {
		return shipaddress;
	}
	public void setShipaddress(String shipaddress) {
		this.shipaddress = shipaddress;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		JSONArray json=JSONArray.fromObject(this);
		return json.toString();
	}
}
