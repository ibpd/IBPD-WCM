package com.ibpd.henuocms.web.controller.test;


import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.TemplateUtil;
import com.ibpd.henuocms.test.OAMailEntity;
import com.ibpd.henuocms.web.controller.manage.BaseController;

@Controller
public class TestController extends BaseController {
	private List<OAMailEntity> mailList=null;
	@RequestMapping("/testTag.do")
	public String testTag(){
		return "tag"; 
	}
	@RequestMapping("/tst.do")
	public void test(Model model,HttpServletRequest req,HttpServletResponse resp,String userName,String password) throws IOException, IllegalAccessException{
		TemplateUtil tu=new TemplateUtil();
		String r=tu.getWebRoot()+"WEB-INF"+File.separator+"jsp"+File.separator+"template"+File.separator+"template.jsp";
		System.out.println(r);
	}
}
