package com.ibpd.henuocms.web.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.hsqldb.lib.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.common.TemplateUtil;
import com.ibpd.henuocms.entity.ContentEntity;
import com.ibpd.henuocms.entity.NodeAttrEntity;
import com.ibpd.henuocms.entity.NodeEntity;
import com.ibpd.henuocms.entity.PageTemplateEntity;
import com.ibpd.henuocms.entity.StyleTemplateEntity;
import com.ibpd.henuocms.entity.SubSiteEntity;
import com.ibpd.henuocms.service.content.ContentServiceImpl;
import com.ibpd.henuocms.service.content.IContentService;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
import com.ibpd.henuocms.service.nodeAttr.INodeAttrService;
import com.ibpd.henuocms.service.nodeAttr.NodeAttrServiceImpl;
import com.ibpd.henuocms.service.pageTemplate.IPageTemplateService;
import com.ibpd.henuocms.service.pageTemplate.PageTemplateServiceImpl;
import com.ibpd.henuocms.service.styleTemplate.IStyleTemplateService;
import com.ibpd.henuocms.service.styleTemplate.StyleTemplateServiceImpl;
import com.ibpd.henuocms.service.subSite.ISubSiteService;
import com.ibpd.henuocms.service.subSite.SubSiteServiceImpl;
import com.ibpd.henuocms.web.controller.manage.BaseController;
@Controller
public class WebContent  extends BaseController{
	@RequestMapping("Web/Content/index.do")
	public String index(org.springframework.ui.Model model,Long contentId,String type,HttpServletRequest req){
		if(contentId==null){
			model.addAttribute(ERROR_MSG,"内容ID为空");
			return super.ERROR_PAGE;			
		}
		IContentService cntServ=(IContentService) getService(ContentServiceImpl.class);
		ContentEntity cnt=cntServ.getEntityById(contentId);
		if(cnt==null){
			model.addAttribute(ERROR_MSG,"没有找到这个内容");
			return super.ERROR_PAGE;
		}
		INodeService nodeService=(INodeService) getService(NodeServiceImpl.class);
		NodeEntity node=nodeService.getEntityById(cnt.getNodeId());
		if(node==null){
			model.addAttribute(ERROR_MSG,"没有找到内容所在栏目");
			return super.ERROR_PAGE;
		}
		Long nodeId=node.getId();
		//调取模板的逻辑：先调本身的，如果本身有，那么就用本身设置的模板，否则调用上级栏目模板，直到找到模板为止，
		//如果到站点都没有找到，就是用默认的栏目模板
		//样式模板的调用，根据页面模板调用来，调用到那个模板，就是用那个模板的样式
		Long tempId=getPageTemplateId(nodeId);
		if(tempId==-1L){
			ISubSiteService siteService=(ISubSiteService) getService(SubSiteServiceImpl.class);
			tempId=siteService.getPageTemplateId(node.getSubSiteId(), PageTemplateEntity.TEMPLATE_TYPE_CONTENT);
		}
		IPageTemplateService pt=(IPageTemplateService) getService(PageTemplateServiceImpl.class);
		PageTemplateEntity pageTemplate=null;
		if(tempId==-1L){
			pageTemplate=pt.getDefaultPageTemplate(PageTemplateEntity.TEMPLATE_TYPE_NODE);
		}else{
			pageTemplate=pt.getEntityById(tempId); 
		} 
		if(pageTemplate==null){
			model.addAttribute(ERROR_MSG,"没有找到页面模板");
			return super.ERROR_PAGE;
		}
		IStyleTemplateService styleTempService=(IStyleTemplateService) getService(StyleTemplateServiceImpl.class);
		Long styleTempId=getStyleTemplateId(node,pageTemplate);
		StyleTemplateEntity styleTemplate=null;
		if(styleTempId==-1L){
			styleTempId=pageTemplate.getDefaultStyleTemplateId();
		}
		styleTemplate=styleTempService.getEntityById(styleTempId);
		if(styleTemplate==null){
			model.addAttribute(ERROR_MSG,"没有找到样式模板");
			return super.ERROR_PAGE;
		}
		//以上是准备内容的过程，下面是把具体的模板路径等映射到前台的过程
		String tmpPath=pageTemplate.getTemplateFilePath();
		if(StringUtil.isEmpty(tmpPath)){
			model.addAttribute(ERROR_MSG,"模板路径为空");
			return super.ERROR_PAGE;
		}
		ISubSiteService siteServ=(ISubSiteService) getService(SubSiteServiceImpl.class);
		SubSiteEntity currentSite=siteServ.getEntityById(cnt.getSubSiteId());
		String vdir=getVisualPathByType(req,type);
		model.addAttribute("currentSite",currentSite);
		if(currentSite!=null){
			model.addAttribute("siteSmallTitle",currentSite.getSiteName());
			model.addAttribute("siteFullTitle",currentSite.getFullName());
		}
		model.addAttribute(PAGE_TITLE,cnt.getTitle());
		model.addAttribute("pageTitle",cnt.getTitle());
		model.addAttribute("currentContent",cnt);
		model.addAttribute("currentNode",node);
		model.addAttribute("keywords",cnt.getAboutKeyword());
		model.addAttribute("description",cnt.getDescription());
		model.addAttribute("copyright",node.getCopyright()); 
		if(pageTemplate.getUseDarkColor())
			model.addAttribute("useDarkColor",TemplateUtil.getDarkColorCssText());
		if(!pageTemplate.getUseMouseRightKey())
			model.addAttribute("enableMouseRightKey",TemplateUtil.getUseMouseRightKey());
		model.addAttribute("cssPath",vdir+"template/"+styleTemplate.getTemplateFilePath()+"/style.css");
		model.addAttribute("pageTemplatePath",vdir+"template/"+pageTemplate.getTemplateFilePath()+"/");
		model.addAttribute("styleTemplatePath",vdir+"template/"+styleTemplate.getTemplateFilePath()+"/");
		return "template/"+pageTemplate.getTemplateFilePath()+"/index";
	} 
	private Long getPageTemplateId(Long nodeId){
		if(nodeId==null)
			return -1L;
		INodeAttrService nodeAttrService=(INodeAttrService) getService(NodeAttrServiceImpl.class);
		NodeAttrEntity attr=nodeAttrService.getNodeAttr(nodeId);
		if(attr==null)
			return -1L;
		if(attr.getContentpageTemplateId()!=-1L){
			return attr.getContentpageTemplateId();
		}else{
			INodeService ns=(INodeService) getService(NodeServiceImpl.class);
			NodeEntity node=ns.getEntityById(nodeId);
			if(node==null){
				return -1L;
			}else{
				return getPageTemplateId(node.getParentId());
			}
		}
	}
	private Long getStyleTemplateId(NodeEntity node,PageTemplateEntity pageTemplate){
		INodeAttrService nodeAttrService=(INodeAttrService) getService(NodeAttrServiceImpl.class);
		NodeAttrEntity attr=nodeAttrService.getNodeAttr(node.getId());
		if(attr.getContentStyleTemplateId()!=-1){
			return attr.getContentStyleTemplateId();
		}else{
			//先找页面模板设置的样式模板,如果没有则找上级
			if(pageTemplate.getDefaultStyleTemplateId()!=-1){
				return pageTemplate.getDefaultStyleTemplateId();
			}else{
				INodeService ns=(INodeService) getService(NodeServiceImpl.class);
				NodeEntity n=ns.getEntityById(node.getParentId());
				return getStyleTemplateId(n,pageTemplate);
			}
		}
	}
}
